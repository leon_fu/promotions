//
//  TalentForEventTableViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 11/1/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class TalentForEventTableViewController: PFQueryTableViewController {
    var getInvitesForEvent: PFObject! // Should be the event we need to get a list of invites for
    
    // MARK: Init
    
    convenience init(className: String?, event: PFObject!) {
        self.init(style: .Plain, className: className)
        self.getInvitesForEvent = event
        
        title = "Invites"
        pullToRefreshEnabled = true
        objectsPerPage = 20
        paginationEnabled = true
    }
    
    // MARK: Data
    
    override func queryForTable() -> PFQuery {
        return super.queryForTable().whereKey("eventObject", equalTo:getInvitesForEvent!)
    }
    
    // MARK: TableView
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        let cellIdentifier = "cell"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? PFTableViewCell
        if cell == nil {
            cell = PFTableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
        }
        
        let status = object?["status"] as? String
        if (status == "Verified") {
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }

        if (getInvitesForEvent != nil) {
            if let talent = object!["talentObject"] as? PFObject {
                talent.fetchIfNeededInBackgroundWithBlock({ (talent: PFObject?, error: NSError?) -> Void in
                    if (talent != nil) {
                        let talentQuery = PFQuery(className:"Talents")
                        talentQuery.whereKey("User", equalTo: talent!)
                        talentQuery.getFirstObjectInBackgroundWithBlock({ (talent: PFObject?, error: NSError?) -> Void in
                            cell?.textLabel?.text = talent!["talentName"] as? String
                            
                            // Get the status
                            
                            cell?.detailTextLabel?.text = status
                        })
                    }
                })
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let tableViewCell = tableView.cellForRowAtIndexPath(indexPath)
        let status = tableViewCell?.detailTextLabel?.text
        let statusObject = self.objectAtIndexPath(indexPath)
        
        if (status == "Checked In") {
            tableViewCell?.accessoryType = UITableViewCellAccessoryType.Checkmark
            statusObject?["status"] = "Verified"
            statusObject?.saveInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                tableViewCell?.detailTextLabel?.text = "Verified"
            })
        }
    }
}
