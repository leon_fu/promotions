//
//  PlacePickerTableViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 11/15/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

protocol PlacePickerTableViewControllerDelegate : class {
    func placePickerTableViewController(placePickerTableViewController: PlacePickerTableViewController, selectedPlace place: NSDictionary )
}

class PlacePickerTableViewController: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate {

    weak var delegate: PlacePickerTableViewControllerDelegate!
    
    @IBOutlet weak var searchBar: UISearchBar!
    private var locationManager: CLLocationManager!
    private var lastLocation: CLLocation!
    private var places: Array<NSDictionary>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get Our Current Location
        self.startStandardUpdates()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        locationManager.stopUpdatingLocation()
    }
    
    func startStandardUpdates() {
        if locationManager == nil {
            locationManager = CLLocationManager.init()
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500;
        let status = CLLocationManager.authorizationStatus()
        if (status == CLAuthorizationStatus.AuthorizedWhenInUse) {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var timeDifference: NSTimeInterval!
        if (lastLocation != nil) {
            timeDifference = locations.last!.timestamp.timeIntervalSinceDate(lastLocation.timestamp)
        }
        lastLocation = locations.last
        
        if (timeDifference == nil || timeDifference > 60) {
            self.locationsSearch(nil)
        }
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationsSearch(searchText: String?) { // "q": searchText
        let center = "\(lastLocation.coordinate.latitude), \(lastLocation.coordinate.longitude)"
        var parameters = ["type": "place", "distance": String(10000), "limit": String(300), "center": center, "offset": "0"]
        parameters["fields"] = "id, name, location, phone"
        
        if searchText != nil {
            parameters["q"] = searchText
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        FBSDKGraphRequest.init(graphPath: "search", parameters: parameters).startWithCompletionHandler({ (connection: FBSDKGraphRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
            if (error == nil) {
                if result != nil {
                    self.places = (result!["data"] as? NSArray) as? Array
                    self.tableView.reloadData()
                }
            } else {
                // Handle graph error here.
            }
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location Error: \(error)")
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    // MARK: search bar delegates
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.locationsSearch(searchBar.text)
    }
    
    // MARK: table view delegates
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowCount = (self.places == nil) ? 0 : self.places.count
        
        return rowCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
        let place = places[indexPath.row]
        let location = place["location"] as? NSDictionary
        let address = location?.objectForKey("street")?.length > 0 ? location?.objectForKey("street") as! String + ", " : ""
        let city = location?.objectForKey("city") != nil ? location?.objectForKey("city") as! String : ""
        let state = location?.objectForKey("state") != nil ? location?.objectForKey("state") as! String : ""
        let zip = location?.objectForKey("zip") != nil ? location?.objectForKey("zip") as! String : ""
        let phone = place["phone"]?.length > 0 ? "\nPhone: " + (place["phone"] as! String) : ""
        
        cell.textLabel?.text = place["name"] as? String
        cell.detailTextLabel?.text = "\(address)\(city) \(state) \(zip)\(phone)"
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let place = places[indexPath.row]
        self.delegate?.placePickerTableViewController(self, selectedPlace: place)
        self.performSegueWithIdentifier("exitLocationSelectionTableView", sender: self)
    }
}
