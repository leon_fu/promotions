//
//  EventListTableViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 11/1/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class EventListTableViewController: PFQueryTableViewController {
    var selectedEvent: PFObject!
    
    // MARK: Init
    
    convenience init(className: String?) {
        self.init(style: .Plain, className: className)
        
        title = "Events"
        pullToRefreshEnabled = true
        objectsPerPage = 20
        paginationEnabled = true
    }
    
    // MARK: Data
    
    override func queryForTable() -> PFQuery {
        return super.queryForTable().whereKey("createdByUser", equalTo:PFUser.currentUser()!)
    }
    
    // MARK: TableView
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        let cellIdentifier = "cell"
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? PFTableViewCell
        if cell == nil {
            cell = PFTableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
        }
        
        cell?.textLabel?.text = object?["eventName"] as? String
        
        var subtitle: String = ""
        let dateFormatter = NSDateFormatter(); dateFormatter.dateStyle = .ShortStyle; dateFormatter.timeStyle = .ShortStyle
        if let startTime = object?["startTime"] as? NSDate {
            let startTimeFormat = dateFormatter.stringFromDate(startTime)
            subtitle = "Start Time: \(startTimeFormat) : "
        }
        
        if let endTime = object?["endTime"] as? NSDate {
            let endTimeFormat = dateFormatter.stringFromDate(endTime)
            subtitle += "End Time: \(endTimeFormat)"
            cell?.detailTextLabel?.text = subtitle
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        super.tableView(tableView, didSelectRowAtIndexPath: indexPath)
        
        if (self.objects?.count > indexPath.row) {
            self.selectedEvent = self.objects![indexPath.row] as? PFObject
            self.parentViewController?.performSegueWithIdentifier("showEventViewTableViewSegue", sender: self)
        }
    }    
}
