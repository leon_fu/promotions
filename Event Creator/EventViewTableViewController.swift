//
//  EventViewTableViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 10/31/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

enum EventTableViewOperationMode {
    case CreateEvent
    case ViewEvent
}

class EventViewTableViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate, PlacePickerTableViewControllerDelegate {
    var createdNewEvent: PFObject! // New Event that we created
    var eventView: PFObject! // Existing Event to populate screen with.
    var operationMode: EventTableViewOperationMode = .CreateEvent
    var talentStatusTableViewController: TalentForEventTableViewController!
    var venueID: String!
    
    // Form field outlets
    @IBOutlet weak var venueNameTextField: UITextField!
    @IBOutlet weak var eventNameTextField: UITextField!
    @IBOutlet weak var startTimeDatePicker: UIDatePicker!
    @IBOutlet weak var endTimeDatePicker: UIDatePicker!
    @IBOutlet weak var offerTypeTextField: UITextField!
    @IBOutlet weak var numberInvitesTextField: UITextField!
    @IBOutlet weak var inviteStepper: UIStepper!
    @IBOutlet weak var offerDetailsTextView: UITextView!
    @IBOutlet weak var instructionsTextView: UITextView!
    @IBOutlet weak var inviteMoreButton: UIButton!
    @IBOutlet weak var createEventButton: UIButton!
    @IBOutlet weak var invitedTalentCell: UITableViewCell!
    @IBOutlet weak var talentStatusView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberInvitesTextField.text = String(format:"%.0f", inviteStepper.value)
        
        if self.operationMode  == .CreateEvent {
            self.inviteMoreButton.hidden = true
        } else { // We're in event view mode; setup for viewing
            self.createEventButton.hidden = true
            
            // Populate data fields.
            self.venueNameTextField.text = self.eventView.objectForKey("venueName") as? String
            self.eventNameTextField.text = self.eventView.objectForKey("eventName") as? String
            self.startTimeDatePicker.date = self.eventView.objectForKey("startTime") as! NSDate
            self.endTimeDatePicker.date = self.eventView.objectForKey("endTime") as! NSDate
            self.offerDetailsTextView.text = self.eventView.objectForKey("offerDetails") as? String
            self.inviteStepper.value  = self.eventView.objectForKey("maxNumberInvites") as! Double
            self.numberInvitesTextField.text = String(format:"%.0f", inviteStepper.value)
            self.offerDetailsTextView.text = self.eventView.objectForKey("offerDetails") as? String
            self.instructionsTextView.text = self.eventView.objectForKey("instructions") as? String
            
            // Add the tableView for list of invited talent
            talentStatusTableViewController = TalentForEventTableViewController.init(className: "InvitedTalentForEvents", event: self.eventView)
            talentStatusTableViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
            talentStatusTableViewController.view.frame = self.talentStatusView.bounds
            self.talentStatusView.addSubview(talentStatusTableViewController.view)
        }
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    @IBAction func inviteStepperClicked(sender: UIStepper) {
        self.numberInvitesTextField.text = String(format:"%.0f", inviteStepper.value)
    }
        
    @IBAction func inviteMoreClicked(sender: UIButton) {
        // To invite more talent, we can do what we do for create event, set the event we created, dismiss and bring up the talent view
        self.createdNewEvent = eventView
        
        // Exit out to previous menu
        self.performSegueWithIdentifier("createEventClicked", sender: self)
    }
    
    @IBAction func createEventButtonClicked(sender: UIButton) {
        // We need to construct our event and write it out to Parse here:
        let event = PFObject(className: "Events")
        
        event["venueId"] = venueID
        event["createdByUser"] = PFUser.currentUser()
        event["venueName"] = venueNameTextField.text
        event["eventName"] = eventNameTextField.text
        event["startTime"] = startTimeDatePicker.date
        event["endTime"] = endTimeDatePicker.date
        event["offerType"] = offerTypeTextField.text
        event["maxNumberInvites"] = inviteStepper.value
        event["offerDetails"] = offerDetailsTextView.text
        event["instructions"] = instructionsTextView.text

        event.saveInBackground()
        self.createdNewEvent = event
        
        // Exit out to previous menu
        self.performSegueWithIdentifier("createEventClicked", sender: self)
    }
    
    @IBAction func cancelButtonClicked(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true) // Cancel, just pop us.
    }
    
    @IBAction func exitLocationSelectionTableView(segue: UIStoryboardSegue) {
        print("exitLocationSelectionTableView")
    }

    // MARK: - Table view delegate
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)
        if self.operationMode == .CreateEvent { // Hide the invite section if we're in create event mode
            if cell == self.invitedTalentCell {
                return 0
            }
        }

        return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
    
    //     MARK: - Navigation
    
    //     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "presentPlacePickerTableViewController" {
            let placePickerVC = (segue.destinationViewController as! UINavigationController).topViewController as! PlacePickerTableViewController
            placePickerVC.delegate = self
        }
        
    }
    
    func placePickerTableViewController(placePickerTableViewController: PlacePickerTableViewController, selectedPlace place: NSDictionary ) {
        venueID = place["id"] as? String
        venueNameTextField.text = place["name"] as? String
    }
    // MARK: - Table view data source

//    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }

//    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

}
