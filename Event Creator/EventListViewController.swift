//
//  EventListViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 10/31/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class EventListViewController: UIViewController {
    // This object is set after the EventViewTableVC creates the event and is dimissed. Then is passed to Talent Selection VC
    var createdEventItem: PFObject?
    
    @IBOutlet weak var addEventButton: UIButton!
    @IBOutlet weak var tableViewContainerView: UIView!
    let eventListTableView = EventListTableViewController.init(className: "Events")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChildViewController(self.eventListTableView)
        self.eventListTableView.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.eventListTableView.view.frame = self.tableViewContainerView.frame
        self.tableViewContainerView.addSubview(self.eventListTableView.view)
        self.eventListTableView.didMoveToParentViewController(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.createdEventItem != nil {
            self.performSegueWithIdentifier("showTalentSelectionSegue", sender:self)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showTalentSelectionSegue" {
            if let talentVC = segue.destinationViewController as? TalentSelectionViewController {
                 // we are going to the talent screen right after creating an event here.
                talentVC.eventObject = self.createdEventItem
            }
        } else if segue.identifier == "showEventViewTableViewSegue" {
            if let eventViewTableViewVC = segue.destinationViewController as? EventViewTableViewController {
                eventViewTableViewVC.operationMode = .ViewEvent
                eventViewTableViewVC.eventView = eventListTableView.selectedEvent
            }
        }
    }

    @IBAction func createEventClicked(segue: UIStoryboardSegue) {
        if let eventVC = segue.sourceViewController as? EventViewTableViewController {
            self.createdEventItem = eventVC.createdNewEvent
        }
    }

    @IBAction func doneTalentSelection(segue: UIStoryboardSegue) { // Called when finished with talent selection
        self.createdEventItem = nil // We're done selecting talent for this event, so nil out createdEventItem and refresh
        eventListTableView.loadObjects()
    }

}