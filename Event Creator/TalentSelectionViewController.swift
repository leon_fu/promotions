//
//  TalentSelectionViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 11/1/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class TalentSelectionViewController: UIViewController {

    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var profileInfo: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
//    private var talentCount = -1
    var eventObject: PFObject! // This is the event we're selecting talent for
    private var currentTalent: PFUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageControl.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
        
        self.loadRandomTalent()
    }
    
//    func getTalentCount() {
//        let talentQueryCount = PFQuery(className:"Talents")
//        talentQueryCount.countObjectsInBackgroundWithBlock({ (count: Int32, error:NSError?) -> Void in
//            self.talentCount = Int(count)
//            self.loadRandomTalent()
//        })
//    }
    
    // Pulls a random talent from Talent Table and populate the screen
    func loadRandomTalent() {
//        if (self.talentCount > 0) {
            let invitedTalent = PFQuery(className: "InvitedTalentForEvents")
            invitedTalent.whereKey("eventObject", equalTo: eventObject)
            invitedTalent.findObjectsInBackgroundWithBlock({ (invitedTalent: [PFObject]?, error: NSError?) -> Void in
                var talents = [PFUser]()
                for talentInvited in invitedTalent! {
                    let talent = talentInvited["talentObject"] as! PFUser
                    talents.append(talent)
                }
                // talents is everyone already invited. Don't invite them again.
                let talentQuery = PFQuery(className:"Talents")
                talentQuery.whereKey("User", notContainedIn: talents)
                talentQuery.getFirstObjectInBackgroundWithBlock({ (object:PFObject?, error:NSError?) -> Void in
                    if object == nil {
                        self.profileInfo.text = "No More Talent"
                        self.photoLabel.text = nil
                        self.currentTalent = nil
                    } else {
                        self.currentTalent = object?["User"] as? PFUser
                        // Load the Talent into the screen here:
                        self.profileInfo.text = object?["talentName"] as? String
                        self.photoLabel.text = object?["talentPhoto"] as? String
                    }
                })
            })
            
//        }
    }
    
    @IBAction func doneButtonClicked(sender: UIButton) {
        
    }
    
    @IBAction func yesClicked(sender: UIButton) {
        // Add invite for self.eventObject
        if (self.currentTalent != nil) {
            let invitedTalent = PFObject(className: "InvitedTalentForEvents")
            invitedTalent["eventObject"] = eventObject
            invitedTalent["talentObject"] = self.currentTalent
            invitedTalent["status"] = "Invited"
            self.currentTalent = nil
            
            invitedTalent.saveInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                self.loadRandomTalent() // Load next talent
            })
        }
        
    }
    
    @IBAction func noClicked(sender: UIButton) {
        self.loadRandomTalent() // Load next talent
    }

}
