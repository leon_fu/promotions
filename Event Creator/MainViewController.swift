//
//  MainViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 10/31/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let accessToken = FBSDKAccessToken.currentAccessToken()
        if (accessToken == nil) { // If we haven't logged in, segue to the login page.
            self.performSegueWithIdentifier("loginViewControllerSegue", sender: self)
        } else { // Login with the token if we haven't already.
            if SharedVariables.sharedInstance.userLoggedIn == nil {
                PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken, block: { (user: PFUser?, error: NSError?) -> Void in
                    SharedVariables.sharedInstance.userLoggedIn = user
                    
                    if user == nil {
                        print("Uh oh. There was an error logging in.")
                    } else {
                        self.performSegueWithIdentifier("showEventListViewController", sender:self)
                    }
                })
            } else {
                self.performSegueWithIdentifier("showEventListViewController", sender:self)
            }
        }
    }
    
    // MARK: - Navigation
    
    @IBAction func loginDone(segue: UIStoryboardSegue) {
        let fbProfile = SharedVariables.sharedInstance.userLoggedIn
        
        // Verify we got everything
        print("User Id:" + fbProfile.username!)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showEventListViewController" {
            
        }
    }

}
