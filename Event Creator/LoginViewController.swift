//
//  ViewController.swift
//  Event Creator
//
//  Created by Leon Fu on 10/30/15.
//  Copyright © 2015 Leon Fu. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: - FBSDKLoginButtonDelegate
    
    @IBAction func fbLoginButtonClicked(sender: UIButton) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile", "user_about_me", "email", "user_friends"]) {
            (user: PFUser?, error: NSError?) -> Void in
            
            SharedVariables.sharedInstance.userLoggedIn = user
            if user == nil {
                print("Uh oh. The user cancelled the Facebook login.")
            }
            
            self.performSegueWithIdentifier("ExitLogin", sender: self)
        }
    }
}

// Sample code to do Graph request:
// Testing Graph request
//                    FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, bio, age_range, gender"]).startWithCompletionHandler({ (connection: FBSDKGraphRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
//                        if (error == nil) {
//                            if result != nil {
//                                self.dict = result as! NSDictionary
//                                print(self.dict)
//                                var userDescription = "Id:" + (self.dict.objectForKey("id") as? String)!
//                                userDescription = userDescription + "\n Email:" + (self.dict.objectForKey("email") as? String)!
//                                userDescription = userDescription + "\n Gender:" + (self.dict.objectForKey("gender") as? String)!
//                                userDescription = userDescription + "\n age_range_min:" + (self.dict.objectForKey("age_range")?.objectForKey("min") as! NSNumber).stringValue
//                                userDescription = userDescription + "\n Bio:" + (self.dict.objectForKey("bio") as? String)!
//                                //                        if let WebURL: String = self.dict.objectForKey("picture")?.objectForKey("data")?.objectForKey("url") as? String {
//                                //                            let image = UIImage.init(data: NSData.init(contentsOfURL: NSURL(string:WebURL)!)!)
//                                //                            self.imageView.image = image
//                                //                        }
//
//
//                                print(userDescription)
//                            }
//                        } else {
//                            // Handle graph error here.
//                        }
//                    })